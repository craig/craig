# Craig Barrett

## Who I am

Hello, I'm Craig :wave:

I am a DevOpsy/SRE-ish shaped person, and I (usually) focus on infrastructure management and automation.

## Where I am

As with many people in this day and age, I can be found in a seemingly endless list of places around the net; here are a few that seem most pertinent.

- GitLab: [`@craig`](https://gitlab.com/craig)
- Github: [`@ctbarrett`](https://github.com/ctbarrett)
- LinkedIn: [ctbarrett](https://linkedin.com/in/ctbarrett)
